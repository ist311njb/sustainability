package ist311njb.sustainability;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.crashlytics.android.Crashlytics;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Profile extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private int userid;
    private TextView usernameView;
    private TextView pointsView;
    private SwipeRefreshLayout postsRefresh;
    private SwipeRefreshLayout jobsRefresh;
    private static final String baseURL = "http://70.15.145.40:1234/ist311/v1/";
    MyRequestQueue mRequestQueue;
    private HashMap<Integer, Post> posts;
    private HashMap<Integer, Job> jobs;
    private GetPostsTask getPostsTask;
    private GetJobsTask getJobsTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        usernameView = findViewById(R.id.profile_username);
        pointsView = findViewById(R.id.profile_points);
        postsRefresh = findViewById(R.id.swiperefresh);
        postsRefresh.setOnRefreshListener(this);
        jobsRefresh = findViewById(R.id.swiperefresh2);
        jobsRefresh.setOnRefreshListener(this);
        posts = new HashMap<>();
        jobs = new HashMap<>();
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            userid = extras.getInt("ID");
        }
        mRequestQueue = MyRequestQueue.getInstance(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        TabLayout tabLayout = findViewById(R.id.tablayout);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int pos = tab.getPosition();
                if(pos == 0){
                    findViewById(R.id.swiperefresh2).setVisibility(View.GONE);
                    findViewById(R.id.swiperefresh).setVisibility(View.VISIBLE);
                }else if(pos == 1){
                    findViewById(R.id.swiperefresh).setVisibility(View.GONE);
                    findViewById(R.id.swiperefresh2).setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        setUpPage();
    }

    private void setUpPage() {
        SharedPreferences settings = getSharedPreferences("UserInfo", 0);
        int id = settings.getInt("userid", 0);
        if(id == userid){
            String username = settings.getString("Username", null);
            usernameView.setText(username);
            ImageView imageView = findViewById(R.id.profile_image);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Profile.this, ChangeProfileImage.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.putExtra("ID", userid);
                    startActivity(intent);
                    finish();
                }
            });
            getPoints();
        }else{
            getUserInfo();
        }
        getPostsTask = new GetPostsTask(userid, this);
        getPostsTask.doInBackground();
        getJobsTask = new GetJobsTask(userid, this);
        getJobsTask.doInBackground();
        GetProfileImageTask imageTask = new GetProfileImageTask(userid);
        imageTask.doInBackground();
    }


    private void getUserInfo() {
        JSONObject requestBody = new JSONObject();
        try {
            requestBody.put("userid", userid);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, baseURL + "user", requestBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("R", response.toString());
                try {
                    if(response.getBoolean("error")){

                    }else{
                        String username = response.getString("username");
                        int points = response.getInt("points");
                        usernameView.setText(username);
                        pointsView.setText(points + " Points");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("E", error.toString());
                Crashlytics.logException(error);
            }
        });
        mRequestQueue.addToRequestQueue(request);
    }

    private void getPoints() {
        JSONObject requestBody = new JSONObject();
        try {
            requestBody.put("userid", userid);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, baseURL + "user/jobs/points", requestBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("R", response.toString());
                try {
                    if(response.getBoolean("error")){

                    }else{
                        int points = response.getInt("points");
                        pointsView.setText(points + " Points");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("E", error.toString());
                Crashlytics.logException(error);
            }
        });
        mRequestQueue.addToRequestQueue(request);
    }

    public class GetPostsTask extends AsyncTask<Void, Void, Boolean>{

        private final int mUserID;
        private final Context mContext;

        public GetPostsTask(int userid, Context c){
            mUserID = userid;
            mContext = c;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            JSONObject requestBody = new JSONObject();
            try {
                requestBody.put("userid", mUserID);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, baseURL + "user/posts", requestBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("R", response.toString());
                    List<Integer> idList = new ArrayList<>();
                    List<Integer> userIDList = new ArrayList<>();
                    List<String> titleList = new ArrayList<>();
                    List<String> contentsList = new ArrayList<>();
                    List<String> authorList = new ArrayList<>();
                    List<Integer> likesList = new ArrayList<>();
                    List<Integer> commentsList = new ArrayList<>();
                    try {
                        if(response.getBoolean("error")){
                            TextView noPostsView = findViewById(R.id.noposts);
                            noPostsView.setVisibility(View.VISIBLE);
                            return;
                        }
                        JSONArray ids = (JSONArray) response.get("id");
                        JSONArray titles = (JSONArray) response.get("title");
                        JSONArray contents = (JSONArray) response.get("content");
                        JSONArray authors = (JSONArray) response.get("username");
                        JSONArray likes = (JSONArray) response.get("likes");
                        JSONArray comments = (JSONArray) response.get("comments");
                        for(int i = 0; i < ids.length(); i++){
                            idList.add(ids.getInt(i));
                            userIDList.add(mUserID);
                            titleList.add(titles.getString(i));
                            contentsList.add(contents.getString(i));
                            authorList.add(authors.getString(i));
                            likesList.add(likes.getInt(i));
                            commentsList.add(comments.getInt(i));
                        }
                        createPosts(idList, titleList, contentsList, authorList, likesList, commentsList, userIDList);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Crashlytics.logException(e);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("E", error.toString());
                    Crashlytics.logException(error);
                    TextView noPostsView = findViewById(R.id.noposts);
                    noPostsView.setVisibility(View.VISIBLE);
                }
            });
            mRequestQueue.addToRequestQueue(request);
            return true;
        }

        public void createPosts(List<Integer> ids, List<String> titles, List<String> contents, List<String> authors, List<Integer> likes, List<Integer> comments, List<Integer> userIDList){
            TextView noPostsView = findViewById(R.id.noposts);
            if(noPostsView != null) {
                noPostsView.setVisibility(View.GONE);
            }
            LinearLayout list = findViewById(R.id.list);
            list.removeAllViews();
            for(int i = 0; i < ids.size(); i++){
                Post post = new Post(mContext, ids.get(i), titles.get(i), contents.get(i), authors.get(i), likes.get(i), comments.get(i), userIDList.get(i));
                list.addView(post);
                posts.put(ids.get(i), post);
            }
            getUserLikes();
        }
        private void getUserLikes() {
            JSONObject requestBody = new JSONObject();
            SharedPreferences settings = getSharedPreferences("UserInfo", 0);
            int thisUser = settings.getInt("userid", 0);
            try {
                requestBody.put("userid", thisUser);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, baseURL + "user/likes", requestBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("R", response.toString());
                    List<Integer> likedPosts = new ArrayList<>();
                    try {
                        if(response.getBoolean("error")){
                            return;
                        }else{
                            JSONArray likes = (JSONArray) response.get("posts");
                            for(int i = 0; i < likes.length(); i++){
                                likedPosts.add(likes.getInt(i));
                            }
                            setLikedPosts(likedPosts);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("E", error.toString());
                    Crashlytics.logException(error);
                }
            });
            mRequestQueue.addToRequestQueue(request);
        }
        private void setLikedPosts(List<Integer> likedPosts) {
            for(int i: likedPosts){
                if(posts.containsKey(i)) {
                    Post p = posts.get(i);
                    p.setLikeState(LikeState.LIKED);
                }
            }
        }
    }

    public class GetJobsTask extends AsyncTask<Void, Void, Boolean>{

        private final int mUserID;
        private final Context mContext;

        public GetJobsTask(int mUserID, Context mContext) {
            this.mUserID = mUserID;
            this.mContext = mContext;
        }


        @Override
        protected Boolean doInBackground(Void... voids) {
            JSONObject requestBody = new JSONObject();
            try {
                requestBody.put("userid", mUserID);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, baseURL + "user/jobs/full", requestBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("R", response.toString());
                    List<Integer> idList = new ArrayList<>();
                    List<Integer> userIDList = new ArrayList<>();
                    List<Integer> rewardList = new ArrayList<>();
                    List<String> titleList = new ArrayList<>();
                    List<String> descriptionList = new ArrayList<>();
                    List<String> authorList = new ArrayList<>();
                    try {
                        if(response.getBoolean("error")){
                            TextView noJobsView = findViewById(R.id.nojobs);
                            noJobsView.setVisibility(View.VISIBLE);
                            return;
                        }
                        JSONArray ids = response.getJSONArray("id");
                        JSONArray userids = response.getJSONArray("userid");
                        JSONArray titles = response.getJSONArray("title");
                        JSONArray descriptions = response.getJSONArray("description");
                        JSONArray rewards = response.getJSONArray("reward");
                        JSONArray usernames = response.getJSONArray("username");
                        for(int i = 0; i < ids.length(); i++){
                            idList.add(ids.getInt(i));
                            userIDList.add(userids.getInt(i));
                            rewardList.add(rewards.getInt(i));
                            titleList.add(titles.getString(i));
                            descriptionList.add(descriptions.getString(i));
                            authorList.add(usernames.getString(i));
                        }
                        createJobs(idList, userIDList, rewardList, titleList, descriptionList, authorList);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("E", error.toString());
                    Crashlytics.logException(error);
                }
            });
            mRequestQueue.addToRequestQueue(request);
            return true;
        }
        private void createJobs(List<Integer> idList, List<Integer> userIDList, List<Integer> rewardList, List<String> titleList, List<String> descriptionList, List<String> authorList) {
            TextView noJobsView = findViewById(R.id.nojobs);
            if(noJobsView != null) {
                noJobsView.setVisibility(View.VISIBLE);
            }
            LinearLayout list = findViewById(R.id.list2);
            list.removeAllViews();
            for(int i = 0; i < idList.size(); i++){
                Job job = new Job(mContext, idList.get(i), userIDList.get(i), rewardList.get(i), titleList.get(i), descriptionList.get(i), authorList.get(i));
                list.addView(job);
                jobs.put(idList.get(i), job);
            }
            getUserCompleteJobs();
        }
        private void getUserCompleteJobs(){
            SharedPreferences settings = getSharedPreferences("UserInfo", 0);
            int thisUser = settings.getInt("userid", 0);
            JSONObject requestBody = new JSONObject();
            try {
                requestBody.put("userid", thisUser);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, baseURL + "user/jobs", requestBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("R", response.toString());
                    List<Integer> completedJobs = new ArrayList<>();
                    try {
                        if(response.getBoolean("error")){

                        }else{
                            JSONArray jobs = response.getJSONArray("id");
                            for(int i = 0; i < jobs.length(); i++){
                                completedJobs.add(jobs.getInt(i));
                            }
                            setCompletedJobs(completedJobs);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("E", error.toString());
                    Crashlytics.logException(error);
                }
            });
            mRequestQueue.addToRequestQueue(request);
        }

        private void setCompletedJobs(List<Integer> completedJobs) {
            for(int i : completedJobs){
                if(jobs.containsKey(i)) {
                    Job j = jobs.get(i);
                    j.setCompleted();
                }
            }
        }
    }

    public class GetProfileImageTask extends AsyncTask<Void, Void, Boolean>{

        private final int mUserid;

        public GetProfileImageTask(int id){
            mUserid = id;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            JSONObject requestBody = new JSONObject();
            try {
                requestBody.put("userid", mUserid);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, baseURL + "user/image", requestBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("R", response.toString());
                    String encodedString = null;
                    try {
                        if(!response.getBoolean("error")) {
                            encodedString = response.getString("image");
                            encodedString = encodedString.replace("\\", "");
                            //Log.d("D", encodedString);
                            byte[] decodedString = Base64.decode(encodedString, Base64.URL_SAFE);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            ImageView imageView = findViewById(R.id.profile_image);
                            imageView.setImageBitmap(decodedByte);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("E", error.toString());
                    Crashlytics.logException(error);
                }
            });
            mRequestQueue.addToRequestQueue(request);
            return true;
        }
    }


    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    @Override
    public void onRefresh() {
        if(postsRefresh.isRefreshing()){
            Log.d("D", "Posts refresh called");
            posts.clear();
            getPostsTask.doInBackground();
            postsRefresh.setRefreshing(false);
        }else if(jobsRefresh.isRefreshing()){
            Log.d("D", "Jobs refresh called");
            jobs.clear();
            getJobsTask.doInBackground();
            jobsRefresh.setRefreshing(false);
        }
    }

    public void refreshPosts(){
        posts.clear();
        getPostsTask.doInBackground();
    }

    public void refreshJobs(){
        jobs.clear();
        getJobsTask.doInBackground();
    }
}
