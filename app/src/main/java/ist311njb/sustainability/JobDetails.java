package ist311njb.sustainability;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.view.View.GONE;

public class JobDetails extends AppCompatActivity {

    private int jobid;
    private MyRequestQueue mRequestQueue;
    private static final String baseURL = "http://70.15.145.40:1234/ist311/v1/";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_details);
       /* Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar); */

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle extras = getIntent().getExtras();
        if(extras != null){
           jobid = extras.getInt("ID");
        }
        mRequestQueue = MyRequestQueue.getInstance(this);
        Button complete = findViewById(R.id.complete);
        complete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                completeJob();
            }
        });
        getJob();
    }

    private void getJob() {
        JSONObject requestBody = new JSONObject();
        try {
            requestBody.put("jobid", jobid);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, baseURL + "job", requestBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("R", response.toString());
                try {
                    if(response.getBoolean("error")){

                    }else{
                        String title = response.getString("title");
                        String author = response.getString("username");
                        String description = response.getString("description");
                        int reward = response.getInt("reward");
                        int userid = response.getInt("userid");
                        TextView t = findViewById(R.id.title);
                        t.setText(title);
                        TextView a = findViewById(R.id.author);
                        a.setText("Author: " + author);
                        a.setTextColor(Color.BLUE);
                        a.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(JobDetails.this, Profile.class);
                                intent.putExtra("ID", userid);
                                startActivity(intent);
                            }
                        });
                        TextView d = findViewById(R.id.description);
                        d.setText(description);
                        TextView r = findViewById(R.id.reward);
                        r.setText(reward + " Points");
                        getUserCompleteJobs();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("E", error.toString());
                Crashlytics.logException(error);
            }
        });

        mRequestQueue.addToRequestQueue(request);
    }

    private void completeJob() {
        SharedPreferences settings = getSharedPreferences("UserInfo", 0);
        int thisUser = settings.getInt("userid", 0);
        JSONObject requestBody = new JSONObject();
        try {
            requestBody.put("jobid", jobid);
            requestBody.put("userid", thisUser);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, baseURL + "job/complete", requestBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("D", response.toString());
                try {
                    if(response.getBoolean("error")){
                        Answers.getInstance().logCustom(new CustomEvent("Complete Job").putCustomAttribute("Success", "Error"));
                    }else{
                        Answers.getInstance().logCustom(new CustomEvent("Complete Job").putCustomAttribute("Success", "Success"));
                        setCompleted();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("E", error.toString());
                Crashlytics.logException(error);
            }
        });
        mRequestQueue.addToRequestQueue(request);
    }

    public void setCompleted(){
        Button complete = findViewById(R.id.complete);
        complete.setVisibility(GONE);
        TextView c = findViewById(R.id.job_complete);
        c.setVisibility(View.VISIBLE);
    }

    private void getUserCompleteJobs(){
        SharedPreferences settings = getSharedPreferences("UserInfo", 0);
        int thisUser = settings.getInt("userid", 0);
        JSONObject requestBody = new JSONObject();
        try {
            requestBody.put("userid", thisUser);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, baseURL + "user/jobs", requestBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("R", response.toString());
                List<Integer> completedJobs = new ArrayList<>();
                try {
                    if(response.getBoolean("error")){

                    }else{
                        JSONArray jobs = response.getJSONArray("id");
                        for(int i = 0; i < jobs.length(); i++){
                            completedJobs.add(jobs.getInt(i));
                        }
                        setCompletedJobs(completedJobs);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("E", error.toString());
                Crashlytics.logException(error);
            }
        });
        mRequestQueue.addToRequestQueue(request);
    }

    private void setCompletedJobs(List<Integer> completedJobs) {
        if(completedJobs.contains(jobid)){
            setCompleted();
        }
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

}
