package ist311njb.sustainability;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;

public class PostDetails extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, Likeable {

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private int postid;
    private Button delete = null;
    private MyRequestQueue mRequestQueue;
    private static final String baseURL = "http://70.15.145.40:1234/ist311/v1/";
    private Context mContext;
    private LikeState likeState = LikeState.NOT_LIKED;
    private ImageButton likeButton;
    private Resources res;
    private Drawable likeImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if(!Fabric.isInitialized()){
            Fabric.with(this, new Crashlytics());
        }
        mRequestQueue = MyRequestQueue.getInstance(this);
        mSwipeRefreshLayout = findViewById(R.id.swiperefresh);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mContext = this;
        Bundle extras = getIntent().getExtras();
        Button postComment = findViewById(R.id.action_post_comment);
        postComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createComment();
            }
        });
        if(extras != null){
            postid = extras.getInt("ID");
        }
        res = getResources();
        likeButton = findViewById(R.id.action_like);
        likeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeLikeState();
            }
        });
        getPost();
    }

    public void getPost(){
        JSONObject requestBody = new JSONObject();
        try {
            requestBody.put("postid", postid);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, baseURL + "post", requestBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("R", response.toString());
                try {
                    String title = response.getString("title");
                    String contents = response.getString("contents");
                    String author = response.getString("username");
                    int likes = response.getInt("likes");
                    int commentCount = response.getInt("commentCount");
                    int userid = response.getInt("userid");
                    TextView t = findViewById(R.id.title);
                    t.setText(title);
                    TextView a = findViewById(R.id.author);
                    a.setText("Author: " + author);
                    a.setTextColor(Color.BLUE);
                    a.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(PostDetails.this, Profile.class);
                            intent.putExtra("ID", userid);
                            startActivity(intent);
                        }
                    });
                    TextView c = findViewById(R.id.contents);
                    c.setText(contents);
                    TextView l = findViewById(R.id.likes);
                    l.setText(likes + " Likes");
                    TextView cc = findViewById(R.id.commentCount);
                    cc.setText(commentCount + " Comments");
                    SharedPreferences settings = getSharedPreferences("UserInfo", 0);
                    int thisUser = settings.getInt("userid", 0);
                    int isAdmin = settings.getInt("isAdmin", 0);
                    LinearLayout post = findViewById(R.id.post_layout);
                    if(delete != null) {
                        post.removeView(delete);
                    }
                    if(thisUser == response.getInt("userid") || isAdmin == 1){
                        delete = new Button(mContext);
                        delete.setText("Delete Post");
                        delete.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                new AlertDialog.Builder(mContext).setTitle("Confirm Delete").setMessage("Are you sure you want to delete this post?").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        post.removeView(delete);
                                        removePost();
                                    }
                                }).setNegativeButton("No", null).show();
                            }
                        });
                    }
                    if(delete != null) {
                        post.addView(delete);
                    }

                    if(commentCount > 0){
                    JSONObject comments = (JSONObject) response.get("comments");
                    if(comments != null) {
                        JSONArray ids = (JSONArray) comments.get("id");
                        JSONArray content = (JSONArray) comments.get("contents");
                        JSONArray usernames = (JSONArray) comments.get("username");
                        JSONArray userids = (JSONArray) comments.get("userids");
                        List<Integer> idList = new ArrayList<>();
                        List<String> contentList = new ArrayList<>();
                        List<String> usernameList = new ArrayList<>();
                        List<Integer> useridList = new ArrayList<>();
                        for (int i = 0; i < ids.length(); i++) {
                            idList.add(ids.getInt(i));
                            contentList.add(content.getString(i));
                            usernameList.add(usernames.getString(i));
                            useridList.add(userids.getInt(i));
                        }
                        createComments(idList, contentList, usernameList, useridList);
                    }
                    getUserLikes();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("E", error.toString());
                Crashlytics.logException(error);
            }
        });

        mRequestQueue.addToRequestQueue(request);
    }


    private void changeLikeState() {
        if(likeState.equals(LikeState.NOT_LIKED)){
            this.likeState = LikeState.LIKED;
            addLike();
            setLikeState(LikeState.LIKED);
        }else if(likeState.equals(LikeState.LIKED)){
            this.likeState = LikeState.NOT_LIKED;
            removeLike();
            setLikeState(LikeState.NOT_LIKED);
        }
    }

    private void removePost() {
        JSONObject requestBody = new JSONObject();
        try {
            requestBody.put("postid", postid);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, baseURL + "post/remove", requestBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("D", response.toString());
                try {
                    if(response.getBoolean("error")){
                        Answers.getInstance().logCustom(new CustomEvent("Delete Post").putCustomAttribute("Succss", "Error"));
                    }else{
                        Answers.getInstance().logCustom(new CustomEvent("Delete Post").putCustomAttribute("Success", "Success"));
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("E", error.toString());
                Crashlytics.logException(error);
            }
        });
        mRequestQueue.addToRequestQueue(request);
    }

    private void createComments(List<Integer> idList, List<String> contentList, List<String> usernameList, List<Integer> useridList) {
        LinearLayout comments = findViewById(R.id.comments);
        comments.removeAllViews();
        for(int i = 0; i < idList.size(); i++){
            Comment comment = new Comment(this, idList.get(i), usernameList.get(i), contentList.get(i), useridList.get(i));
            comments.addView(comment);
        }

    }

    private void createComment(){
        String comment = "";
        boolean cancel = false;
        View focusView = null;
        AutoCompleteTextView text = findViewById(R.id.text_comment);
        text.setError(null);
        comment = text.getText().toString();
        if(TextUtils.isEmpty(comment)){
            text.setError(getString(R.string.error_field_required));
            cancel = true;
            focusView = text;
        }
        if(cancel){
            focusView.requestFocus();
        }else{
            SharedPreferences settings = getSharedPreferences("UserInfo", 0);
            int userID = settings.getInt("userid", 0);
            JSONObject requestBody = new JSONObject();
            try {
                requestBody.put("contents", comment);
                requestBody.put("postid", postid);
                requestBody.put("userid", userID);
                Log.d("T", "Contents: " + comment + "  Post ID: " + postid + "  User ID: " + userID);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, baseURL + "post/comments/add", requestBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("R", response.toString());
                    try {
                        if(response.getBoolean("error")){
                            Answers.getInstance().logCustom(new CustomEvent("New Comment").putCustomAttribute("Success", "Error"));
                            Snackbar snack = Snackbar.make(findViewById(R.id.action_post_comment), "Error posting comment, Try Again Later", Snackbar.LENGTH_LONG).setAction("Action", null);
                            View view = snack.getView();
                            TextView tv = view.findViewById(com.google.android.material.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snack.show();
                        }else{
                            Answers.getInstance().logCustom(new CustomEvent("New Comment").putCustomAttribute("Success", "Success"));
                            text.setText(null);
                            getPost();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("E", error.toString());
                    Crashlytics.logException(error);
                }
            });
            mRequestQueue.addToRequestQueue(request);
        }
    }

    private void getUserLikes() {
        JSONObject requestBody = new JSONObject();
        SharedPreferences settings = getSharedPreferences("UserInfo", 0);
        int thisUser = settings.getInt("userid", 0);
        try {
            requestBody.put("userid", thisUser);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, baseURL + "user/likes", requestBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("R", response.toString());
                List<Integer> likedPosts = new ArrayList<>();
                try {
                    if(response.getBoolean("error")){
                        return;
                    }else{
                        JSONArray likes = (JSONArray) response.get("posts");
                        for(int i = 0; i < likes.length(); i++){
                            likedPosts.add(likes.getInt(i));
                        }
                        setLiked(likedPosts);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("E", error.toString());
                Crashlytics.logException(error);
            }
        });
        mRequestQueue.addToRequestQueue(request);
    }

    private void setLiked(List<Integer> likedPosts) {
        if(likedPosts.contains(postid)){
            setLikeState(LikeState.LIKED);
        }else{
            setLikeState(LikeState.NOT_LIKED);
        }
    }

    @Override
    public void onRefresh() {
        Log.d("D", "Swipe refresh called");
        LinearLayout post = findViewById(R.id.post_layout);
        post.removeView(delete);
        getPost();
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void setLikeState(LikeState likeState) {
        this.likeState = likeState;
        if(likeState == LikeState.LIKED) {
            likeImage = res.getDrawable(R.drawable.thumb_up, getTheme());
            likeButton.setImageDrawable(likeImage);
        }else if(likeState == LikeState.NOT_LIKED){
            likeImage = res.getDrawable(R.drawable.thumb_up_outline, getTheme());
            likeButton.setImageDrawable(likeImage);
        }
    }

    private void removeLike() {
        SharedPreferences settings = getSharedPreferences("UserInfo", 0);
        int thisUser = settings.getInt("userid", 0);
        JSONObject requestBody = new JSONObject();
        try {
            requestBody.put("postid", postid);
            requestBody.put("userid", thisUser);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, baseURL + "post/likes/remove", requestBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("D", response.toString());
                try {
                    if(response.getBoolean("error")){
                        Answers.getInstance().logCustom(new CustomEvent("Unlike Post").putCustomAttribute("Success", "Error"));
                    }else{
                        Answers.getInstance().logCustom(new CustomEvent("Unlike Post").putCustomAttribute("Success", "Success"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                getPost();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("E", error.toString());
                Crashlytics.logException(error);
            }
        });
        mRequestQueue.addToRequestQueue(request);
    }

    private void addLike() {
        SharedPreferences settings = getSharedPreferences("UserInfo", 0);
        int thisUser = settings.getInt("userid", 0);
        JSONObject requestBody = new JSONObject();
        try {
            requestBody.put("postid", postid);
            requestBody.put("userid", thisUser);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, baseURL + "post/likes/add", requestBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("D", response.toString());
                try {
                    if(response.getBoolean("error")){
                        Answers.getInstance().logCustom(new CustomEvent("Like Post").putCustomAttribute("Success", "Error"));
                    }else{
                        Answers.getInstance().logCustom(new CustomEvent("Like Post").putCustomAttribute("Success", "Success"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                getPost();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("E", error.toString());
                Crashlytics.logException(error);
            }
        });
        mRequestQueue.addToRequestQueue(request);

    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
}
