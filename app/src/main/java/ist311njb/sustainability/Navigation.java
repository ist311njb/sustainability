package ist311njb.sustainability;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import 	androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.crashlytics.android.Crashlytics;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import io.fabric.sdk.android.Fabric;

public class Navigation extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private FirebaseAnalytics mFirebaseAnalytics;
    private int isStaff;
    private static final String baseURL = "http://70.15.145.40:1234/ist311/v1/";
    private MyRequestQueue mRequestQueue;


    protected void onCreateNavigation() {
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        if(!Fabric.isInitialized()){
            Fabric.with(this, new Crashlytics());
        }
        NavigationView navigationView = findViewById(R.id.nav_view);
        mRequestQueue = MyRequestQueue.getInstance(this);
        SharedPreferences settings = getSharedPreferences("UserInfo", 0);
        String fname = settings.getString("Fname", "Default");
        String lname = settings.getString("Lname", "Name");
        isStaff = settings.getInt("isStaff", 0);
        TextView navName = (TextView) navigationView.getHeaderView(0).findViewById(R.id.nav_name);
        navName.setText(fname + " " + lname);
        navigationView.getHeaderView(0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Navigation.this, Profile.class);
                int userid = settings.getInt("userid", 0);
                intent.putExtra("ID", userid);
                startActivity(intent);
            }
        });

        /*setContentView(R.layout.activity_navigation);*/
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        if(isStaff == 1){
            MenuItem newJob = navigationView.getMenu().findItem(R.id.nav_newjob);
            newJob.setVisible(true);
        }
        navigationView.setNavigationItemSelectedListener(this);
        GetProfileImageTask imageTask = new GetProfileImageTask();
        imageTask.doInBackground();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        else if(id == R.id.action_logout){
            logout();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        SharedPreferences settings = getSharedPreferences("UserInfo", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.remove("userid");
        editor.remove("Username");
        editor.remove("Fname");
        editor.remove("Lname");
        editor.remove("isStaff");
        editor.putBoolean("loggedIn", false);
        editor.commit();
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "2");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "Logout");
        mFirebaseAnalytics.logEvent("logout", bundle);
        AuthUI.getInstance()
                .signOut(this)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    public void onComplete(@NonNull Task<Void> task) {
                        Intent intent = new Intent(Navigation.this, LoginScreen.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                });

    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_posts) {
            Intent intent = new Intent(this, Posts.class);
            startActivity(intent);
        } else if (id == R.id.nav_newpost) {
            Intent intent = new Intent(this, NewPost.class);
            startActivity(intent);
        } else if (id == R.id.nav_jobs) {
            Intent intent = new Intent(this, Jobs.class);
            startActivity(intent);
        } else if (id == R.id.nav_newjob){
            Intent intent = new Intent(this, NewJob.class);
            startActivity(intent);
        } else if (id == R.id.nav_password){
            Intent intent = new Intent(this, ChangePassword.class);
            startActivity(intent);
        }else if(id == R.id.nav_logout){
            logout();
        }else if(id == R.id.nav_test){
            Intent intent = new Intent(this, TestActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public class GetProfileImageTask extends AsyncTask<Void, Void, Boolean> {


        @Override
        protected Boolean doInBackground(Void... voids) {
            SharedPreferences settings = getSharedPreferences("UserInfo", 0);
            int userid = settings.getInt("userid", 0);
            JSONObject requestBody = new JSONObject();
            try {
                requestBody.put("userid", userid);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, baseURL + "user/image", requestBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("R", response.toString());
                    String encodedString = null;
                    try {
                        if(!response.getBoolean("error")) {
                            encodedString = response.getString("image");
                            encodedString = encodedString.replace("\\", "");
                            //Log.d("D", encodedString);
                            byte[] decodedString = Base64.decode(encodedString, Base64.URL_SAFE);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            ImageView imageView = findViewById(R.id.nav_profile_image);
                            imageView.setImageBitmap(decodedByte);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("E", error.toString());
                    Crashlytics.logException(error);
                }
            });
            mRequestQueue.addToRequestQueue(request);
            return true;
        }
    }
}
