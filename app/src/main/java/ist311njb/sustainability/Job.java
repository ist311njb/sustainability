package ist311njb.sustainability;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Brady on 11/13/2017.
 */

public class Job extends CardView{

    private int jobid;
    private int userid;
    private int reward;
    private String title;
    private String description;
    private String username;
    private Context context;
    private MyRequestQueue mRequestQueue;
    private static final String baseURL = "http://70.15.145.40:1234/ist311/v1/";
    private Button complete;
    private AppCompatTextView r;
    private LinearLayout layout;

    public Job(Context context, int jobid, int userid, int reward, String title, String description, String username) {
        super(context);
        this.context = context;
        this.jobid = jobid;
        this.userid = userid;
        this.reward = reward;
        this.title = title;
        this.description = description;
        this.username = username;
        mRequestQueue = MyRequestQueue.getInstance(context);
        createContents();
    }

    private void createContents() {
        AppCompatTextView t = new AppCompatTextView(context);
        t.setText(title);
        t.setTextAlignment(TEXT_ALIGNMENT_CENTER);
        AppCompatTextView a = new AppCompatTextView(context);
        a.setText("Author: " + username);
        a.setTextAlignment(TEXT_ALIGNMENT_CENTER);
        a.setTextColor(Color.BLUE);
        a.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, Profile.class);
                intent.putExtra("ID", userid);
                context.startActivity(intent);
            }
        });
        r = new AppCompatTextView(context);
        r.setText("Reward: " + reward + " Points");
        r.setTextAlignment(TEXT_ALIGNMENT_CENTER);
        AppCompatTextView d = new AppCompatTextView(context);
        d.setText(description);
        d.setTextAlignment(TEXT_ALIGNMENT_CENTER);
        complete = new Button(context);
        complete.setText(R.string.complete_job);
        complete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                completeJob();
            }
        });
        layout = new LinearLayout(context);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(15,15,15,15);
        this.setLayoutParams(params);
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.addView(t);
        layout.addView(a);
        layout.addView(r);
        layout.addView(d);
        layout.addView(complete);
        addView(layout);
        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, JobDetails.class);
                intent.putExtra("ID", jobid);
                context.startActivity(intent);
            }
        });
    }

    private void completeJob() {
        SharedPreferences settings = context.getSharedPreferences("UserInfo", 0);
        int thisUser = settings.getInt("userid", 0);
        JSONObject requestBody = new JSONObject();
        try {
            requestBody.put("jobid", jobid);
            requestBody.put("userid", thisUser);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, baseURL + "job/complete", requestBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("D", response.toString());
                try {
                    if(response.getBoolean("error")){
                        Answers.getInstance().logCustom(new CustomEvent("Complete Job").putCustomAttribute("Success", "Error"));
                    }else{
                        Answers.getInstance().logCustom(new CustomEvent("Complete Job").putCustomAttribute("Success", "Success"));
                        complete.setText("Job Completed!");
                        complete.setEnabled(false);
                        r.setText(reward + " Points Collected!");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("E", error.toString());
                Crashlytics.logException(error);
            }
});
        mRequestQueue.addToRequestQueue(request);
        }

    public void setCompleted(){
        complete.setVisibility(GONE);
        AppCompatTextView c = new AppCompatTextView(context);
        c.setText(R.string.job_complete);
        c.setTextAlignment(TEXT_ALIGNMENT_CENTER);
        layout.addView(c);
    }
}
