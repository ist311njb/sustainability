package ist311njb.sustainability;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by Brady on 10/24/2017.
 */

public class MyRequestQueue {
    private static MyRequestQueue mInstance;
    private RequestQueue mRequestQueue;
    private static Context mContext;

    private MyRequestQueue(Context c){
        mContext = c;
        mRequestQueue = getRequestQueue();
    }

    public static synchronized MyRequestQueue getInstance(Context c){
        if(mInstance == null){
            mInstance = new MyRequestQueue(c);
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if(mRequestQueue == null){
            mRequestQueue = Volley.newRequestQueue(mContext.getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req){
        getRequestQueue().add(req);
    }
}
