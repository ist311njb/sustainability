package ist311njb.sustainability;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.crashlytics.android.Crashlytics;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Jobs extends Navigation implements SwipeRefreshLayout.OnRefreshListener{

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private static final String baseURL = "http://70.15.145.40:1234/ist311/v1/";
    MyRequestQueue mRequestQueue;
    private HashMap<Integer, Job> jobs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jobs);
        super.onCreateNavigation();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);
        mSwipeRefreshLayout = findViewById(R.id.swiperefresh);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Jobs.this, NewJob.class);
                startActivity(intent);

            }
        });
        SharedPreferences settings = getSharedPreferences("UserInfo", 0);
        int isStaff = settings.getInt("isStaff", 0);
        if(isStaff == 1){
            fab.setVisibility(View.VISIBLE);
        }
        mRequestQueue = MyRequestQueue.getInstance(this);
        jobs = new HashMap<>();
        getJobs();
    }

    @Override
    public void onRestart(){
        super.onRestart();
        getJobs();
    }

    private void getJobs() {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, baseURL + "jobs", null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("R", response.toString());
                List<Integer> idList = new ArrayList<>();
                List<Integer> userIDList = new ArrayList<>();
                List<Integer> rewardList = new ArrayList<>();
                List<String> titleList = new ArrayList<>();
                List<String> descriptionList = new ArrayList<>();
                List<String> authorList = new ArrayList<>();
                try {
                    if(response.getBoolean("error")){
                        TextView noJobsView = findViewById(R.id.nojobs);
                        noJobsView.setVisibility(View.VISIBLE);
                        return;
                    }
                    JSONArray ids = response.getJSONArray("id");
                    JSONArray userids = response.getJSONArray("userid");
                    JSONArray titles = response.getJSONArray("title");
                    JSONArray descriptions = response.getJSONArray("description");
                    JSONArray rewards = response.getJSONArray("reward");
                    JSONArray usernames = response.getJSONArray("username");
                    for(int i = 0; i < ids.length(); i++){
                        idList.add(ids.getInt(i));
                        userIDList.add(userids.getInt(i));
                        rewardList.add(rewards.getInt(i));
                        titleList.add(titles.getString(i));
                        descriptionList.add(descriptions.getString(i));
                        authorList.add(usernames.getString(i));
                    }
                    createJobs(idList, userIDList, rewardList, titleList, descriptionList, authorList);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("E", error.toString());
                Crashlytics.logException(error);
            }
        });
        mRequestQueue.addToRequestQueue(request);
    }

    private void createJobs(List<Integer> idList, List<Integer> userIDList, List<Integer> rewardList, List<String> titleList, List<String> descriptionList, List<String> authorList) {
        TextView noJobsView = findViewById(R.id.nojobs);
        if(noJobsView != null) {
            noJobsView.setVisibility(View.VISIBLE);
        }
        LinearLayout list = findViewById(R.id.list);
        list.removeAllViews();
        for(int i = 0; i < idList.size(); i++){
            Job job = new Job(this, idList.get(i), userIDList.get(i), rewardList.get(i), titleList.get(i), descriptionList.get(i), authorList.get(i));
            list.addView(job);
            jobs.put(idList.get(i), job);
        }
        getUserCompleteJobs();
    }

    private void getUserCompleteJobs(){
        SharedPreferences settings = getSharedPreferences("UserInfo", 0);
        int thisUser = settings.getInt("userid", 0);
        JSONObject requestBody = new JSONObject();
        try {
            requestBody.put("userid", thisUser);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, baseURL + "user/jobs", requestBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("R", response.toString());
                List<Integer> completedJobs = new ArrayList<>();
                try {
                    if(response.getBoolean("error")){

                    }else{
                        JSONArray jobs = response.getJSONArray("id");
                        for(int i = 0; i < jobs.length(); i++){
                            completedJobs.add(jobs.getInt(i));
                        }
                        setCompletedJobs(completedJobs);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("E", error.toString());
                Crashlytics.logException(error);
            }
        });
        mRequestQueue.addToRequestQueue(request);
    }

    private void setCompletedJobs(List<Integer> completedJobs) {
        for(int i : completedJobs){
            Job j = jobs.get(i);
            j.setCompleted();
        }
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(false);
        jobs.clear();
        getJobs();
    }
}
