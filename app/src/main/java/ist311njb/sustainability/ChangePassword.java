package ist311njb.sustainability;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.crashlytics.android.Crashlytics;
import com.google.android.material.snackbar.Snackbar;
import com.google.common.hash.Hashing;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;

public class ChangePassword extends AppCompatActivity {
    //TODO: Rework to change firebase email password

    private EditText mOldPassword;
    private EditText newPassword;
    private EditText confirmPassword;
    private static final String baseURL = "http://70.15.145.40:1234/ist311/v1/";
    MyRequestQueue mRequestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mRequestQueue = MyRequestQueue.getInstance(this);
        mOldPassword = findViewById(R.id.oldPassword);
        newPassword = findViewById(R.id.newPassword);
        confirmPassword = findViewById(R.id.confirmPassword);
        Button changePasswordButton = findViewById(R.id.changePassword);
        changePasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptChange();
            }
        });
    }

    private void attemptChange(){
        boolean cancel = false;
        View focusView = null;

        mOldPassword.setError(null);
        newPassword.setError(null);
        confirmPassword.setError(null);

        String o = Hashing.sha256().hashString(mOldPassword.getText().toString(), StandardCharsets.UTF_8).toString();
        String n = Hashing.sha256().hashString(newPassword.getText().toString(), StandardCharsets.UTF_8).toString();
        String c = Hashing.sha256().hashString(confirmPassword.getText().toString(), StandardCharsets.UTF_8).toString();

        if(TextUtils.isEmpty(confirmPassword.getText())){
            confirmPassword.setError(getString(R.string.error_field_required));
            cancel = true;
            focusView = confirmPassword;
        } if (TextUtils.isEmpty(newPassword.getText())){
            newPassword.setError(getString(R.string.error_field_required));
            cancel = true;
            focusView = newPassword;
        }if(!TextUtils.equals(n, c)){
            confirmPassword.setError(getString(R.string.error_password_match));
            cancel = true;
            focusView = confirmPassword;
        }if(TextUtils.isEmpty(mOldPassword.getText())){
            mOldPassword.setError(getString(R.string.error_field_required));
            cancel = true;
            focusView = mOldPassword;
        }

        if(cancel){
            focusView.requestFocus();
        }else{
            changePassword(o, n);
        }
    }

    private void changePassword(String oldPassword, String newPassword){
        SharedPreferences settings = getSharedPreferences("UserInfo", 0);
        String username = settings.getString("Username", "");
        JSONObject requestBody = new JSONObject();
        try {
            requestBody.put("username", username);
            requestBody.put("oldPassword", oldPassword);
            requestBody.put("newPassword", newPassword);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, baseURL + "user/changepassword", requestBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("R", response.toString());
                try {
                    if(response.getBoolean("error")){
                        if(response.getString("message").equals("Current password is incorrect")){
                            mOldPassword.setError(getString(R.string.error_incorrect_password));
                            mOldPassword.requestFocus();
                        }else{
                            Snackbar snack = Snackbar.make(findViewById(R.id.changePassword), "Error changing password", Snackbar.LENGTH_LONG).setAction("Action", null);
                            View view = snack.getView();
                            TextView tv = view.findViewById(com.google.android.material.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snack.show();
                        }
                    }else{
                        Toast.makeText(getBaseContext(), "Password Changed Successfully",
                                Toast.LENGTH_LONG).show();
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("E", error.toString());
                Crashlytics.logException(error);
                Snackbar snack = Snackbar.make(findViewById(R.id.changePassword), "Error changing password", Snackbar.LENGTH_LONG).setAction("Action", null);
                View view = snack.getView();
                TextView tv = view.findViewById(com.google.android.material.R.id.snackbar_text);
                tv.setTextColor(Color.RED);
                snack.show();
            }
        });
        mRequestQueue.addToRequestQueue(request);
    }


    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

}
