package ist311njb.sustainability;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

public class NewPost extends AppCompatActivity {

    private static final String baseURL = "http://70.15.145.40:1234/ist311/v1/";
    MyRequestQueue mRequestQueue;
    CreatePostTask mCreatePostTask;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        setContentView(R.layout.activity_new_post);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Button createPost = findViewById(R.id.action_post);
        createPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createNewPost();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mRequestQueue = MyRequestQueue.getInstance(this);
    }

    public void createNewPost(){
        TextView titleView = findViewById(R.id.title);
        TextView contentsView = findViewById(R.id.contents);
        titleView.setError(null);
        contentsView.setError(null);
        View focusView = null;
        boolean cancel = false;
        String title = titleView.getText().toString();
        String contents = contentsView.getText().toString();
        if(TextUtils.isEmpty(contents)){
            contentsView.setError(getString(R.string.error_field_required));
            focusView = contentsView;
            cancel = true;
        }
        if(TextUtils.isEmpty(title)){
            titleView.setError(getString(R.string.error_field_required));
            focusView = titleView;
            cancel = true;
        }

        if(cancel){
            focusView.requestFocus();
        }else{
            mCreatePostTask = new CreatePostTask(title, contents);
            mCreatePostTask.execute((Void) null);
        }
    }

    public class CreatePostTask extends AsyncTask<Void, Void, Boolean>{

        private final String title;
        private final String contents;

        public CreatePostTask(String t, String c){
            title = t;
            contents = c;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            SharedPreferences settings = getSharedPreferences("UserInfo", 0);
            int userID = settings.getInt("userid", 0);
            if(userID == 0){
                return false;
            }

            JSONObject requestBody = new JSONObject();
            try {
                requestBody.put("title", title);
                requestBody.put("content", contents);
                requestBody.put("userid", userID);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, baseURL + "posts/create", requestBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("R", response.toString());
                    try {
                        if(response.getBoolean("error")){
                            Log.e("E", "Error creating post");
                            Snackbar snack = Snackbar.make(findViewById(R.id.action_post), "Error creating post, try again later", Snackbar.LENGTH_LONG).setAction("Action", null);
                            View view = snack.getView();
                            TextView tv = view.findViewById(com.google.android.material.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snack.show();
                        }else{
                            Bundle bundle = new Bundle();
                            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "5");
                            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "User Posted");
                            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SHARE, bundle);
                            Answers.getInstance().logCustom(new CustomEvent("New Post Event").putCustomAttribute("Title", title).putCustomAttribute("Contents", contents));
                            Intent intent = new Intent(NewPost.this, Posts.class);
                            startActivity(intent);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Crashlytics.logException(e);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("E", error.toString());
                    Crashlytics.logException(error);
                    Snackbar snack = Snackbar.make(findViewById(R.id.action_post), "Connection Error, Try Again Later", Snackbar.LENGTH_LONG).setAction("Action", null);
                    View view = snack.getView();
                    TextView tv = view.findViewById(com.google.android.material.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snack.show();
                }
            });
            mRequestQueue.addToRequestQueue(request);

            return true;
        }
    }

}
