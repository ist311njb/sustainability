package ist311njb.sustainability;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Brady on 10/19/2017.
 */

public class Post extends CardView implements Likeable {

    private int postID;
    private int userID;
    private String title;
    private String contents;
    private String username;
    private int likes;
    private int comments;
    private Context context;
    private MyRequestQueue mRequestQueue;
    private LikeState likeState = LikeState.NOT_LIKED;
    private ImageButton likeButton;
    private Resources res;
    private Drawable likeImage;
    private static final String baseURL = "http://70.15.145.40:1234/ist311/v1/";

    public Post(Context c, int id, String t, String content, String u, int l, int com, int uid){
        super(c);
        context = c;
        postID = id;
        userID = uid;
        title = t;
        contents = content;
        username = u;
        likes = l;
        comments = com;
        mRequestQueue = MyRequestQueue.getInstance(c);
        res = getResources();
        createContents();
    }

    private void createContents(){
        AppCompatTextView t = new AppCompatTextView(context);
        t.setText(title);
        t.setTextAlignment(TEXT_ALIGNMENT_CENTER);
        AppCompatTextView c = new AppCompatTextView(context);
        c.setText(contents);
        c.setTextAlignment(TEXT_ALIGNMENT_CENTER);
        AppCompatTextView u = new AppCompatTextView(context);
        u.setText("Author: " + username);
        u.setTextAlignment(TEXT_ALIGNMENT_CENTER);
        u.setTextColor(Color.BLUE);
        u.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, Profile.class);
                intent.putExtra("ID", userID);
                context.startActivity(intent);
            }
        });
        SharedPreferences settings = context.getSharedPreferences("UserInfo", 0);
        int thisUser = settings.getInt("userid", 0);
        int isAdmin = settings.getInt("isAdmin", 0);
        Button delete = null;
        if(thisUser == userID || isAdmin == 1){
            delete = new Button(context);
            delete.setText(R.string.delete_post);
            delete.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    new AlertDialog.Builder(context).setTitle("Confirm Delete").setMessage("Are you sure you want to delete this post?").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            removePost();
                        }
                    }).setNegativeButton("No", null).show();
                }
            });
        }
        LinearLayout layout = new LinearLayout(context);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(15,15,15,15);
        this.setLayoutParams(params);
        layout.setOrientation(LinearLayout.VERTICAL);
        LinearLayout bottom = new LinearLayout(context);
        LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params2.setMargins(50, 50,50,0);
        bottom.setLayoutParams(params2);
        bottom.setOrientation(LinearLayout.HORIZONTAL);
        likeButton = new ImageButton(context);
        likeImage = res.getDrawable(R.drawable.thumb_up_outline, context.getTheme());
        likeButton.setImageDrawable(likeImage);
        likeButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                changeLikeState();
            }
        });
        AppCompatTextView l = new AppCompatTextView(context);
        l.setText(likes + " Likes");
        l.setGravity(Gravity.BOTTOM);
        l.setPadding(10,0,0,0);
        AppCompatTextView comment = new AppCompatTextView(context);
        comment.setText(comments + " Comments");
        comment.setTextColor(Color.BLUE);
        comment.setGravity(Gravity.BOTTOM);
        comment.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, PostDetails.class);
                intent.putExtra("ID", postID);
                context.startActivity(intent);
            }
        });
        bottom.addView(likeButton);
        bottom.addView(l);
        bottom.addView(comment);
        bottom.setGravity(Gravity.BOTTOM);
        layout.addView(t);
        layout.addView(u);
        layout.addView(c);
        layout.addView(bottom);
        if(delete != null) {
            layout.addView(delete);
        }
        this.addView(layout);
        LinearLayout.LayoutParams params3 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params3.setMargins(0,0,400,0);
        l.setLayoutParams(params3);
        this.setForegroundGravity(Gravity.CENTER);
    }

    private void changeLikeState() {
        if(likeState.equals(LikeState.NOT_LIKED)){
            this.likeState = LikeState.LIKED;
            addLike();
        }else if(likeState.equals(LikeState.LIKED)){
            this.likeState = LikeState.NOT_LIKED;
            removeLike();
        }
    }

    private void removePost() {
        JSONObject requestBody = new JSONObject();
        try {
            requestBody.put("postid", postID);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, baseURL + "post/remove", requestBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("D", response.toString());
                try {
                    if(response.getBoolean("error")){
                        Answers.getInstance().logCustom(new CustomEvent("Delete Post").putCustomAttribute("Success", "Error"));
                    }else{
                        Answers.getInstance().logCustom(new CustomEvent("Delete Post").putCustomAttribute("Success", "Success"));
                        if(context instanceof Posts) {
                            Posts p = (Posts) context;
                            p.getPosts();
                        }else if (context instanceof  Profile){
                            Profile p = (Profile) context;
                            p.refreshPosts();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("E", error.toString());
                Crashlytics.logException(error);
            }
        });
        mRequestQueue.addToRequestQueue(request);
    }

    public int getID(){
        return postID;
    }

    @Override
    public void setLikeState(LikeState likeState) {
        this.likeState = likeState;
        if(likeState == LikeState.NOT_LIKED){
            likeImage = res.getDrawable(R.drawable.thumb_up_outline, context.getTheme());
            likeButton.setImageDrawable(likeImage);
        }else if(likeState == LikeState.LIKED){
            likeImage = res.getDrawable(R.drawable.thumb_up, context.getTheme());
            likeButton.setImageDrawable(likeImage);
        }
    }

    private void removeLike() {
        SharedPreferences settings = context.getSharedPreferences("UserInfo", 0);
        int thisUser = settings.getInt("userid", 0);
        JSONObject requestBody = new JSONObject();
        try {
            requestBody.put("postid", postID);
            requestBody.put("userid", thisUser);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, baseURL + "post/likes/remove", requestBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("D", response.toString());
                try {
                    if(response.getBoolean("error")){
                        Answers.getInstance().logCustom(new CustomEvent("Unlike Post").putCustomAttribute("Success", "Error"));
                    }else{
                        Answers.getInstance().logCustom(new CustomEvent("Unlike Post").putCustomAttribute("Success", "Success"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if(context instanceof Posts) {
                    Posts p = (Posts) context;
                    p.getPosts();
                }else if(context instanceof Profile){
                    Profile p = (Profile) context;
                    p.refreshPosts();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("E", error.toString());
                Crashlytics.logException(error);
            }
        });
        mRequestQueue.addToRequestQueue(request);
    }

    private void addLike() {
        SharedPreferences settings = context.getSharedPreferences("UserInfo", 0);
        int thisUser = settings.getInt("userid", 0);
        JSONObject requestBody = new JSONObject();
        try {
            requestBody.put("postid", postID);
            requestBody.put("userid", thisUser);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, baseURL + "post/likes/add", requestBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("D", response.toString());
                try {
                    if(response.getBoolean("error")){
                        Answers.getInstance().logCustom(new CustomEvent("Like Post").putCustomAttribute("Success", "Error"));
                    }else{
                        Answers.getInstance().logCustom(new CustomEvent("Like Post").putCustomAttribute("Success", "Success"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if(context instanceof Posts) {
                    Posts p = (Posts) context;
                    p.getPosts();
                }else if(context instanceof Profile){
                    Profile p = (Profile) context;
                    p.refreshPosts();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("E", error.toString());
                Crashlytics.logException(error);
            }
        });
        mRequestQueue.addToRequestQueue(request);

    }
}
