package ist311njb.sustainability;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.crashlytics.android.Crashlytics;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

public class NewJob extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    private int reward;
    private TextView title;
    private TextView description;
    private Spinner spinner;
    private static final String baseURL = "http://70.15.145.40:1234/ist311/v1/";
    MyRequestQueue mRequestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_job);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        spinner = findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.reward_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
        Button button = findViewById(R.id.action_post);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkInput();
            }
        });
        title = findViewById(R.id.title);
        description = findViewById(R.id.description);
        mRequestQueue = MyRequestQueue.getInstance(this);
    }

    private void checkInput(){
        title.setError(null);
        description.setError(null);
        View focusView = null;
        boolean cancel = false;
        String t = title.getText().toString();
        String d = description.getText().toString();

        if(TextUtils.isEmpty(d)){
            description.setError(getString(R.string.error_field_required));
            focusView = description;
            cancel = true;
        }
        if(TextUtils.isEmpty(t)){
            title.setError(getString(R.string.error_field_required));
            focusView = title;
            cancel = true;
        }
        if(cancel){
            focusView.requestFocus();
        }else{
            createJob(t, d);
        }
    }

    private void createJob(String t, String d) {
        SharedPreferences settings = getSharedPreferences("UserInfo", 0);
        int thisUser = settings.getInt("userid", 0);
        JSONObject requestBody = new JSONObject();
        try {
            requestBody.put("title", t);
            requestBody.put("description", d);
            requestBody.put("reward", reward);
            requestBody.put("userid", thisUser);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, baseURL + "jobs/add", requestBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("R", response.toString());
                try {
                    if(response.getBoolean("error")){
                        Log.e("E", "Error creating post");
                        Snackbar snack = Snackbar.make(findViewById(R.id.action_post), "Error creating job, try again later", Snackbar.LENGTH_LONG).setAction("Action", null);
                        View view = snack.getView();
                        TextView tv = view.findViewById(com.google.android.material.R.id.snackbar_text);
                        tv.setTextColor(Color.RED);
                        snack.show();
                    }else{
                        Intent intent = new Intent(NewJob.this, Jobs.class);
                        startActivity(intent);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("E", error.toString());
                Crashlytics.logException(error);
            }
        });
        mRequestQueue.addToRequestQueue(request);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        String selected = (String) adapterView.getItemAtPosition(i);
        reward = Integer.parseInt(selected);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
