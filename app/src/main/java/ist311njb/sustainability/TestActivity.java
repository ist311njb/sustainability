package ist311njb.sustainability;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.crashlytics.android.Crashlytics;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class TestActivity extends Navigation {

    Button select;
    String imageString = null;
    private static final String baseURL = "http://70.15.145.40:1234/ist311/v1/";
    private MyRequestQueue mRequestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        super.onCreateNavigation();
        mRequestQueue = MyRequestQueue.getInstance(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        /*setSupportActionBar(toolbar);*/

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        select = findViewById(R.id.image_select);
        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Choose Picture"), 1);
            }
        });
        Button upload = findViewById(R.id.image_upload);
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadImage();
            }
        });
        Button download = findViewById(R.id.image_load);
        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getImage();
            }
        });
    }

    private void getImage() {
        SharedPreferences settings = getSharedPreferences("UserInfo", 0);
        int userid = settings.getInt("userid", 0);
        JSONObject requestBody = new JSONObject();
        try {
            requestBody.put("userid", userid);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, baseURL + "user/image", requestBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("R", response.toString());
                String encodedString = null;
                try {
                    encodedString = response.getString("image");
                    encodedString = encodedString.replace("\\", "");
                    Log.d("D", encodedString);
                    byte[] decodedString = Base64.decode(encodedString, Base64.URL_SAFE);
                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                    ImageView imageView = findViewById(R.id.image_selected);
                    imageView.setImageBitmap(decodedByte);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("E", error.toString());
                Crashlytics.logException(error);
            }
        });
        mRequestQueue.addToRequestQueue(request);
    }

    private void uploadImage() {
        if(imageString == null){
            return;
        }
        SharedPreferences settings = getSharedPreferences("UserInfo", 0);
        int userid = settings.getInt("userid", 0);
        JSONObject requestBody = new JSONObject();
        try {
            requestBody.put("userid", userid);
            requestBody.put("image", imageString);
            Log.d("D", requestBody.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, baseURL + "user/image/upload", requestBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("R", response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("E", error.toString());
                Crashlytics.logException(error);
            }
        });
        mRequestQueue.addToRequestQueue(request);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(resultCode==RESULT_CANCELED)
        {
            // action cancelled
        }
        if(resultCode==RESULT_OK)
        {
            Uri selectedimg = data.getData();
            File file = new File(selectedimg.getPath());
            InputStream inputStream = null;//You can get an inputStream using any IO API
            try {
                inputStream = getContentResolver().openInputStream(selectedimg);    ;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            byte[] bytes;
            byte[] buffer = new byte[8192];
            int bytesRead;
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            try {
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    output.write(buffer, 0, bytesRead);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            bytes = output.toByteArray();
            String encodedString = Base64.encodeToString(bytes, Base64.URL_SAFE);
            imageString = encodedString;
            Log.d("D", encodedString);
            byte[] decodedString = Base64.decode(encodedString, Base64.URL_SAFE);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            ImageView imageView = findViewById(R.id.image_selected);
            imageView.setImageBitmap(decodedByte);
        }
    }


}
