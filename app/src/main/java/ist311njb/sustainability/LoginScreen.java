package ist311njb.sustainability;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.LoginEvent;
import com.crashlytics.android.answers.SignUpEvent;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.google.common.hash.Hashing;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

import static android.Manifest.permission.READ_CONTACTS;
import static android.view.View.GONE;

/**
 * A login screen that offers login via email/password.
 */
public class LoginScreen extends AppCompatActivity implements LoaderCallbacks<Cursor> {


    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;

    /**
     * A dummy authentication store containing known user names and passwords.
     * TODO: remove after connecting to a real authentication system.
     */
    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "foo@example.com:hello", "bar@example.com:world"
    };

    /**
     * Base URL of REST API
     */
    private static final String baseURL = "http://70.15.145.40:1234/ist311/v2/";
    MyRequestQueue mRequestQueue;
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLinkTask mAuthTask = null;
    private UserRegisterTask mRegisterTask = null;

    // UI references.
    private AutoCompleteTextView mUsernameView;
    private EditText mPasswordView;
    private TextInputLayout mFirstnameView;
    private AutoCompleteTextView mFirstnameTextView;
    private AutoCompleteTextView mLastnameTextView;
    private TextInputLayout mLastnameView;
    private Button mBackButton;
    private View mProgressView;
    private View mLoginFormView;

    private boolean registerDisplay = false;

    private FirebaseAnalytics mFirebaseAnalytics;
    private static final int RC_SIGN_IN = 123;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this) == 0){
            GoogleApiAvailability.getInstance().makeGooglePlayServicesAvailable(this);
        }
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "1");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "App opened");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.APP_OPEN, bundle);
        Fabric.with(this, new Crashlytics());
        SharedPreferences settings = getSharedPreferences("UserInfo", 0);
        if(settings.getBoolean("loggedIn", false)){
            Intent intent = new Intent(LoginScreen.this, Posts.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK );
            startActivity(intent);
        }else {
            setContentView(R.layout.activity_login_screen);

/*        SharedPreferences settings = getSharedPreferences("UserInfo", 0);
        if(settings.getBoolean("loggedIn", false)){
            Intent intent = new Intent(LoginScreen.this, Posts.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK );
            startActivity(intent);
        }
        setContentView(R.layout.activity_login_screen);
        // Set up the login form.
        mUsernameView = findViewById(R.id.email);
        populateAutoComplete();

        mPasswordView = findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLink();
                    return true;
                }
                return false;
            }
        });

        mFirstnameView = findViewById(R.id.fname);
        mFirstnameTextView = findViewById(R.id.fname_text);
        mLastnameView = findViewById(R.id.lname);
        mLastnameTextView = findViewById(R.id.lname_text);

        Button mEmailSignInButton = findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLink();
            }
        });

        Button mRegisterButton = findViewById(R.id.register_button);
        mRegisterButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!registerDisplay) {
                    mEmailSignInButton.setVisibility(GONE);
                    mFirstnameView.setVisibility(View.VISIBLE);
                    mLastnameView.setVisibility(View.VISIBLE);
                    mBackButton.setVisibility(View.VISIBLE);
                    registerDisplay = true;
                }else{
                    attemptRegister();
                }
            }
        });

        mBackButton = findViewById(R.id.back_button);
        mBackButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                mEmailSignInButton.setVisibility(View.VISIBLE);
                mFirstnameView.setVisibility(GONE);
                mLastnameView.setVisibility(GONE);
                mBackButton.setVisibility(GONE);
                registerDisplay = false;
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);*/
            mRequestQueue = MyRequestQueue.getInstance(this);
            List<AuthUI.IdpConfig> providers = Arrays.asList(new AuthUI.IdpConfig.EmailBuilder().build(),
                    new AuthUI.IdpConfig.GoogleBuilder().build());
            startActivityForResult(AuthUI.getInstance().createSignInIntentBuilder().setAvailableProviders(providers).build(), RC_SIGN_IN);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);

            if (resultCode == RESULT_OK) {
                // Successfully signed in
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                try {
                    Log.d("login", user.getDisplayName());
                    Log.d("login", user.getUid());
                }catch(NullPointerException e){
                    Log.w("login", "Could not get Display Name");
                    Log.w("login", e.toString());
                }
                getUserID(user.getUid());
                // ...
            } else {
                // Sign in failed. If response is null the user canceled the
                // sign-in flow using the back button. Otherwise check
                // response.getError().getErrorCode() and handle the error.
                // ...
            }
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        if(GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this) == 0){
            GoogleApiAvailability.getInstance().makeGooglePlayServicesAvailable(this);
        }
    }



    private void populateAutoComplete() {
        if (!mayRequestContacts()) {
            return;
        }

        getLoaderManager().initLoader(0, null, this);
    }

    private boolean mayRequestContacts() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
            Snackbar.make(mUsernameView, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
                        }
                    });
        } else {
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
        }
        return false;
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                populateAutoComplete();
            }
        }
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLink() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mUsernameView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String username = mUsernameView.getText().toString();
        String password = Hashing.sha256().hashString(mPasswordView.getText().toString(), StandardCharsets.UTF_8).toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }else if(TextUtils.isEmpty(password)){
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid username address.
        if (TextUtils.isEmpty(username)) {
            mUsernameView.setError(getString(R.string.error_field_required));
            focusView = mUsernameView;
            cancel = true;
        } /*else if (!isEmailValid(username)) {
            mUsernameView.setError(getString(R.string.error_invalid_email));
            focusView = mUsernameView;
            cancel = true;
        }*/

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            //showProgress(true);
            mAuthTask = new UserLinkTask(username, password);
            mAuthTask.execute((Void) null);
        }
    }

    private void attemptRegister() {
        // Reset errors.
        mUsernameView.setError(null);
        mFirstnameTextView.setError(null);
        mLastnameTextView.setError(null);

        boolean cancel = false;
        View focusView = null;

        String username = mUsernameView.getText().toString();
        String fname = mFirstnameTextView.getText().toString();
        String lname = mLastnameTextView.getText().toString();

        // Check for a valid username address.
        if (TextUtils.isEmpty(username)) {
            mUsernameView.setError(getString(R.string.error_field_required));
            focusView = mUsernameView;
            cancel = true;
        }

        // Check first name
        if(TextUtils.isEmpty(fname)){
            mFirstnameTextView.setError(getString(R.string.error_field_required));
            focusView = mFirstnameTextView;
            cancel = true;
        }

        // Check last name
        if(TextUtils.isEmpty(lname)){
            mLastnameTextView.setError(getString(R.string.error_field_required));
            focusView = mLastnameTextView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user register attempt.
            //showProgress(true);
            mRegisterTask = new UserRegisterTask(username, fname, lname);
            mRegisterTask.execute((Void) null);
        }

    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : GONE);
            mLoginFormView.setVisibility(show ? GONE : View.VISIBLE);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginScreen.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mUsernameView.setAdapter(adapter);
    }


    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLinkTask extends AsyncTask<Void, Void, Boolean> {

        private final String mUsername;
        private final String mPassword;
        private final String mFirebaseID = FirebaseAuth.getInstance().getUid();

        UserLinkTask(String username, String password) {
            mUsername = username;
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            JSONObject requestBody = new JSONObject();
            try {
                requestBody.put("username", mUsername);
                requestBody.put("password", mPassword);
                requestBody.put("firebaseID", mFirebaseID);
            }catch(JSONException e){

            }

            JsonObjectRequest loginInfo = new JsonObjectRequest(Request.Method.POST, baseURL + "user/link" , requestBody, new Response.Listener<JSONObject>(){

                @Override
                public void onResponse(JSONObject response) {
                    Log.d("R", response.toString());
                    try {
                        boolean success = !response.getBoolean("error");
                        if(success){
                            Log.d("Login", "Login Successful");
                            Answers.getInstance().logLogin(new LoginEvent().putMethod("UserLinkTask").putSuccess(true));
                            Bundle bundle = new Bundle();
                            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "3");
                            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "Login");
                            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN, bundle);
                            getUserID(mFirebaseID);
                        }
                        else{
                            Log.e("Login", "Login Failed");
                            String message = response.getString("message");
                            Answers.getInstance().logLogin(new LoginEvent().putMethod("UserLinkTask").putSuccess(false).putCustomAttribute("Error", message));
                            Snackbar snack = Snackbar.make(findViewById(R.id.email_sign_in_button), message, Snackbar.LENGTH_LONG).setAction("Action", null);
                            View view = snack.getView();
                            TextView tv = view.findViewById(com.google.android.material.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snack.show();
                        }
                    }catch(JSONException e){
                        e.printStackTrace();
                        Crashlytics.logException(e);


                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("PIS", error.toString());
                    Answers.getInstance().logLogin(new LoginEvent().putMethod("UserLinkTask").putSuccess(false).putCustomAttribute("Error", error.toString()));
                    Crashlytics.logException(error);
                    Snackbar snack = Snackbar.make(findViewById(R.id.email_sign_in_button), "Connection Error, Try Again Later", Snackbar.LENGTH_LONG).setAction("Action", null);
                    View view = snack.getView();
                    TextView tv = view.findViewById(com.google.android.material.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snack.show();
                }
            }){
                @Override
                protected Map<String, String> getParams(){
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("username", mUsername);
                    params.put("password", mPassword);
                    return params;
                }
            };



            mRequestQueue.addToRequestQueue(loginInfo);

            for (String credential : DUMMY_CREDENTIALS) {
                String[] pieces = credential.split(":");
                if (pieces[0].equals(mUsername)) {
                    // Account exists, return true if the password matches.
                    return pieces[1].equals(mPassword);
                }
            }

            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            //showProgress(false);

            if (success) {
               // finish();
            } else {
                mPasswordView.setError(getString(R.string.error_incorrect_password));
                mPasswordView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }

    public class UserRegisterTask extends AsyncTask<Void, Void, Boolean>{

        private final String mUsername;
        private final String mFname;
        private final String mLName;
        private final String mFirebaseID = FirebaseAuth.getInstance().getUid();

        UserRegisterTask(String username, String fname, String lname) {
            mUsername = username;
            mFname = fname;
            mLName = lname;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            JSONObject requestBody = new JSONObject();
            try{
                requestBody.put("username", mUsername);
                requestBody.put("firebaseID", mFirebaseID);
                requestBody.put("fname", mFname);
                requestBody.put("lname", mLName);
            }catch(JSONException e){

            }

            JsonObjectRequest register = new JsonObjectRequest(Request.Method.POST, baseURL + "user/create", requestBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("R", response.toString());
                    //showProgress(false);
                    try {
                        // Response was received, check for errors creating account
                        if(response.getBoolean("error") == true){
                            if(response.getString("message").equals("Username already exists")){
                                mUsernameView.setError(getString(R.string.error_username_exists));
                                Answers.getInstance().logSignUp(new SignUpEvent().putMethod("UserRegisterTask").putSuccess(false).putCustomAttribute("ErrorType", "Username exists"));
                            }else{
                                Answers.getInstance().logSignUp(new SignUpEvent().putMethod("UserRegisterTask").putSuccess(false).putCustomAttribute("ErrorType", "Generic Error"));
                                Snackbar snack = Snackbar.make(findViewById(R.id.register_button), response.getString("message"), Snackbar.LENGTH_LONG).setAction("Action", null);
                                View view = snack.getView();
                                TextView tv = view.findViewById(com.google.android.material.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snack.show();
                            }
                        }else{
                            //No error was found, continue
                            Answers.getInstance().logSignUp(new SignUpEvent().putMethod("UserRegisterTask").putSuccess(true));
                            Bundle bundle = new Bundle();
                            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "4");
                            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "User Registered");
                            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SIGN_UP, bundle);
                            getUserID(mFirebaseID);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Crashlytics.logException(e);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    Answers.getInstance().logSignUp(new SignUpEvent().putMethod("UserRegisterTask").putSuccess(false).putCustomAttribute("Error Response", error.toString()));
                    Crashlytics.logException(error);
                    Log.e("R", error.toString());
                    Snackbar snack = Snackbar.make(findViewById(R.id.register_button), "Connection Error, Try Again Later", Snackbar.LENGTH_LONG).setAction("Action", null);
                    View view = snack.getView();
                    TextView tv = view.findViewById(com.google.android.material.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snack.show();
                }
            });

            mRequestQueue.addToRequestQueue(register);

            return true;
        }
        @Override
        protected void onCancelled() {
            mRegisterTask = null;
            showProgress(false);
        }

    }

    public void getUserID(String firebaseID){
        JSONObject requestBody = new JSONObject();
        try {
            requestBody.put("firebaseID", firebaseID);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, baseURL + "user/id", requestBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("R", response.toString());
                try {
                    if(response.getBoolean("error") == false){
                        getUserDetails(response.getInt("userid"));
                    }else{

                        ScrollView noFbID = findViewById(R.id.no_fb_id);
                        noFbID.setVisibility(View.VISIBLE);
                        Button linkButton = findViewById(R.id.link);
                        Button dontLinkButton = findViewById(R.id.dont_link);
                        linkButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                ScrollView link = findViewById(R.id.link_form);
                                noFbID.setVisibility(View.GONE);
                                link.setVisibility(View.VISIBLE);
                                Button backButton = findViewById(R.id.link_back);
                                backButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        link.setVisibility(View.GONE);
                                        noFbID.setVisibility(View.VISIBLE);
                                    }
                                });
                                mUsernameView = findViewById(R.id.username);
                                mPasswordView = findViewById(R.id.password_link);
                                Button linkAccount = findViewById(R.id.do_link);
                                linkAccount.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        attemptLink();
                                    }
                                });
                            }
                        });
                        dontLinkButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                ScrollView newUser = findViewById(R.id.new_user);
                                noFbID.setVisibility(GONE);
                                newUser.setVisibility(View.VISIBLE);
                                Button backButton = findViewById(R.id.new_back);
                                backButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        newUser.setVisibility(GONE);
                                        noFbID.setVisibility(View.VISIBLE);
                                    }
                                });
                                mUsernameView = findViewById(R.id.new_username);
                                mFirstnameView = findViewById(R.id.new_fname);
                                mFirstnameTextView = findViewById(R.id.new_fname_text);
                                mLastnameView = findViewById(R.id.new_lname);
                                mLastnameTextView = findViewById(R.id.new_lname_text);
                                Button doRegister = findViewById(R.id.do_new_user);
                                doRegister.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        attemptRegister();
                                    }
                                });
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Crashlytics.logException(e);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Crashlytics.logException(error);

            }
        });
        mRequestQueue.addToRequestQueue(request);
    }

    public void getUserDetails(int id){
        JSONObject requestBody = new JSONObject();
        try {
            requestBody.put("userid", id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, baseURL + "user", requestBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("R", response.toString());
                try {
                    if(response.getBoolean("error") == false){
                        SharedPreferences settings = getSharedPreferences("UserInfo", 0);
                        SharedPreferences.Editor editor = settings.edit();
                        String username = response.getString("username");
                        String fname = response.getString("fname");
                        String lname = response.getString("lname");
                        int staff = response.getInt("isStaff");
                        int admin = response.getInt("isAdmin");
                        editor.putInt("userid", id);
                        editor.putString("Username",username);
                        editor.putString("Fname", fname);
                        editor.putString("Lname", lname);
                        editor.putInt("isStaff", staff);
                        editor.putInt("isAdmin", admin);
                        editor.putBoolean("loggedIn", true);
                        editor.commit();
                        Intent intent = new Intent(LoginScreen.this, Posts.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK );
                        startActivity(intent);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Crashlytics.logException(e);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Crashlytics.logException(error);
            }
        });
        mRequestQueue.addToRequestQueue(request);
    }

}



