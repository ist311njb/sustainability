package ist311njb.sustainability;

/**
 * Created by Brady on 10/31/2017.
 */

public enum LikeState {
    LIKED, NOT_LIKED
}
