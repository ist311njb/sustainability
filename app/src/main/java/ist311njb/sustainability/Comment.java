package ist311njb.sustainability;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Brady on 10/29/2017.
 */

public class Comment extends CardView {

    private int commentID;
    private Context context;
    private String username;
    private int userID;
    private String contents;
    private MyRequestQueue mRequestQueue;
    private static final String baseURL = "http://70.15.145.40:1234/ist311/v1/";

    public Comment(Context c, int id,  String u, String con, int uid){
        super(c);
        context = c;
        commentID = id;
        username = u;
        contents = con;
        userID = uid;
        mRequestQueue = MyRequestQueue.getInstance(c);
        createContents();
    }

    private void createContents() {
        AppCompatTextView user = new AppCompatTextView(context);
        user.setText(username);
        user.setPadding(0,0,0,10);
        AppCompatTextView content = new AppCompatTextView(context);
        content.setText(contents);
        SharedPreferences settings = context.getSharedPreferences("UserInfo", 0);
        int thisUser = settings.getInt("userid", 0);
        int isAdmin = settings.getInt("isAdmin", 0);
        Button delete = null;
        if(thisUser == userID || isAdmin == 1){
            delete = new Button(context);
            delete.setText(R.string.delete_comment);
            delete.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    new AlertDialog.Builder(context).setTitle("Confirm Delete").setMessage("Are you sure you want to delete this comment?").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            removeComment();
                        }
                    }).setNegativeButton("No", null).show();
                }
            });
        }
        LinearLayout layout = new LinearLayout(context);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(15,15,15,15);
        this.setPadding(0,15,0,15);
        layout.setLayoutParams(params);
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.addView(user);
        layout.addView(content);
        if(delete != null){
            layout.addView(delete);
        }
        this.addView(layout);
        this.setForegroundGravity(Gravity.CENTER);
        this.setUseCompatPadding(true);
    }

    private void removeComment(){
        JSONObject requestBody = new JSONObject();
        try {
            requestBody.put("id", commentID);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, baseURL + "post/comments/remove", requestBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("D", response.toString());
                try {
                    if(response.getBoolean("error")){
                        Answers.getInstance().logCustom(new CustomEvent("Comment Deleted").putCustomAttribute("Success", "Error"));
                    }else{
                        Answers.getInstance().logCustom(new CustomEvent("Comment Deleted").putCustomAttribute("Success", "Success"));
                        PostDetails p = (PostDetails) context;
                        p.getPost();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("E", error.toString());
                Crashlytics.logException(error);
            }
        });
        mRequestQueue.addToRequestQueue(request);
    }
}
