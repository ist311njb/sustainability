package ist311njb.sustainability;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.crashlytics.android.Crashlytics;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Posts extends Navigation implements SwipeRefreshLayout.OnRefreshListener {

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private static final String baseURL = "http://70.15.145.40:1234/ist311/v1/";
    MyRequestQueue mRequestQueue;
    private HashMap<Integer, Post> posts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_posts);
        super.onCreateNavigation();
        posts = new HashMap<>();
        mSwipeRefreshLayout = findViewById(R.id.swiperefresh);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Posts.this, NewPost.class);
                startActivity(intent);
            }
        });
        mRequestQueue = MyRequestQueue.getInstance(this);
        getPosts();
    }

    @Override
    public void onRestart(){
        super.onRestart();
        getPosts();
    }

    public void getPosts(){
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, baseURL + "posts", null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("R", response.toString());
                List<Integer> idList = new ArrayList<>();
                List<Integer> userIDList = new ArrayList<>();
                List<String> titleList = new ArrayList<>();
                List<String> contentsList = new ArrayList<>();
                List<String> authorList = new ArrayList<>();
                List<Integer> likesList = new ArrayList<>();
                List<Integer> commentsList = new ArrayList<>();
                try {
                    if(response.getBoolean("error")){
                        TextView noPostsView = findViewById(R.id.noposts);
                        noPostsView.setVisibility(View.VISIBLE);
                        return;
                    }
                    JSONArray ids = (JSONArray) response.get("id");
                    JSONArray userids = (JSONArray) response.get("userid");
                    JSONArray titles = (JSONArray) response.get("title");
                    JSONArray contents = (JSONArray) response.get("content");
                    JSONArray authors = (JSONArray) response.get("username");
                    JSONArray likes = (JSONArray) response.get("likes");
                    JSONArray comments = (JSONArray) response.get("comments");
                    for(int i = 0; i < ids.length(); i++){
                        idList.add(ids.getInt(i));
                        userIDList.add(userids.getInt(i));
                        titleList.add(titles.getString(i));
                        contentsList.add(contents.getString(i));
                        authorList.add(authors.getString(i));
                        likesList.add(likes.getInt(i));
                        commentsList.add(comments.getInt(i));
                    }
                    createPosts(idList, titleList, contentsList, authorList, likesList, commentsList, userIDList);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Crashlytics.logException(e);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("E", error.toString());
                Crashlytics.logException(error);
                TextView noPostsView = findViewById(R.id.noposts);
                noPostsView.setVisibility(View.VISIBLE);
            }
        });
        mRequestQueue.addToRequestQueue(request);
    }

    public void createPosts(List<Integer> ids, List<String> titles, List<String> contents, List<String> authors, List<Integer> likes, List<Integer> comments, List<Integer> userIDList){
        TextView noPostsView = findViewById(R.id.noposts);
        if(noPostsView != null) {
            noPostsView.setVisibility(View.GONE);
        }
        LinearLayout list = findViewById(R.id.list);
        list.removeAllViews();
        for(int i = 0; i < ids.size(); i++){
            Post post = new Post(this, ids.get(i), titles.get(i), contents.get(i), authors.get(i), likes.get(i), comments.get(i), userIDList.get(i));
            list.addView(post);
            posts.put(ids.get(i), post);
        }
        getUserLikes();
    }

    private void getUserLikes() {
        JSONObject requestBody = new JSONObject();
        SharedPreferences settings = getSharedPreferences("UserInfo", 0);
        int thisUser = settings.getInt("userid", 0);
        try {
            requestBody.put("userid", thisUser);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, baseURL + "user/likes", requestBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("R", response.toString());
                List<Integer> likedPosts = new ArrayList<>();
                try {
                    if(response.getBoolean("error")){
                        return;    
                    }else{
                        JSONArray likes = (JSONArray) response.get("posts");
                        for(int i = 0; i < likes.length(); i++){
                            likedPosts.add(likes.getInt(i));
                        }
                        setLikedPosts(likedPosts);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("E", error.toString());
                Crashlytics.logException(error);
            }
        });
        mRequestQueue.addToRequestQueue(request);
    }

    private void setLikedPosts(List<Integer> likedPosts) {
        for(int i: likedPosts){
            Post p = posts.get(i);
            p.setLikeState(LikeState.LIKED);
        }
    }

    @Override
    public void onRefresh() {
        Log.d("D", "Swipe refresh called");
        posts.clear();
        getPosts();
        mSwipeRefreshLayout.setRefreshing(false);
    }
}
